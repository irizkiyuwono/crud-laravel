<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/register', 'AuthController@register');
    
Route::get('/home', 'HomeController@home');

Route::get('/welcomes', 'AuthController@welcomes');

Route::get('/master', function(){
    return view('adminlte.master');
});

Route::get('/datas', function(){
    return view('datas');
});

Route::get('/data-tables', function(){
    return view('data-tables');
});

Route::get('/items', function(){
    return view('items.index');
});

Route::get('/items/create', function(){
    return view('items.create');
});
